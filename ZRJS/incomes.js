function IncomesContainer() {
    var addIncomeHTML = "<table id=\"addIncomeTable\"><tbody><tr><td>Description:</td><td><input id=\"descriptionIncome\" type=\"text\" size=\"19\"></td></tr><tr><td>Recurrence:</td><td><input type=\"radio\" name=\"recIncome\" checked=\"checked\" value=\"None\"> None <input type=\"radio\" name=\"recIncome\" value=\"Monthly\">Monthly<input type=\"radio\" name=\"recIncome\" value=\"Yearly\">Yearly</td></tr><tr><td>Value: </td><td><input id=\"valueIncome\" type=\"text\" value=\"0.00\" size=\"5\" >&nbsp;&nbsp;$</td></tr><tr><td>Note:</td><td><textarea id=\"noteIncome\" maxlength=\"95\"></textarea></td></tr><tr><td colspan=\"2\"><label id=\"calendarLabel\">asd</label></td></tr><tr><td colspan=\"2\"><input id=\"totalSumIncome\" checked=\"checked\" type=\"checkbox\"> Sum to total </td></tr><tr><td colspan=\"2\" align=\"right\"><button id=\"submintAddIncome\">Add income</button><button id=\"cancelAddIncome\">Cancel</button></tr></tbody></table>";
    var numOfNewIncomes = 0;
    // this.appendHTMLStringCode("<button id =\"incomeAddButton\"><label id=\"incomeAddButtonText\">Click here in order to add a new income.</label></button>");
    //REMOVE
    this.getAddIncomeCode = function() {
        return addIncomeHTML;
    };
    this.addNewElementToHTML = function(element) {
        var newChild = document.createElement("DIV");
        var tmp1, tmp2;
        newChild.className = 'anIncome';
        tmp1 = document.createElement("DIV");
        tmp2 = document.createElement("p");
        tmp2.setAttribute("class", "anIncomeP anIncomeDescription");
        tmp2.innerHTML = "<b>Description:</b> " + element.desc;
        tmp1.appendChild(tmp2);
        tmp2 = document.createElement("p");
        tmp2.setAttribute("class", "anIncomeP");
        tmp2.innerHTML = "<b>Recurrence:</b> " + element.recurr;
        tmp1.appendChild(tmp2);
        tmp2 = document.createElement("p");
        tmp2.setAttribute("class", "anIncomeP");
        tmp2.innerHTML = "<b>Value:</b> " + element.value;
        tmp1.appendChild(tmp2);
        newChild.appendChild(tmp1);
        tmp1 = document.createElement("DIV");
        tmp2 = document.createElement('p');
        tmp2.setAttribute("class", "anIncomeNote anIncomeP");
        tmp2.innerHTML = "<b>Note:</b><br>" + element.note;
        tmp1.appendChild(tmp2);
        tmp2 = document.createElement('p');
        tmp2.setAttribute("class", "anIncomeP");
        tmp2.innerHTML = "<b>Date:</b>" + element.date;
        tmp1.appendChild(tmp2);
        newChild.appendChild(tmp1);
        this.addChildNodeToHTML(newChild);
        $('.anIncomeDesc').css('background-color', '#071907');
        $('.anIncomeDesc').css('color', 'white');
    };
    this.getIncomesDIVNode = function() {
        return incomesHTML;
    };
    this.addIncomeButtonClicked = false;
}
IncomesContainer.prototype = new Container();
IncomesContainer.prototype.newIncomeParser = function() {
    var parseDescription = /^(?:[A-Za-z]+)(?: *\w)*$/;
    var parseVale = /^(\d+)(\.\d+)?$/;
    var parseNote = /^(?:[!-\/:-@\[-`{-~\-\s\w]*)$/;
    return function(input, inputType) {
        var result = null;
        switch (inputType) {
            case 0:
                result = parseDescription.exec(input) !== null;
                break;
            case 1:
                result = parseVale.exec(input) !== null;
                break;
            case 2:
                result = parseNote.exec(input) !== null;
                break;
        }
        return result;
    }
}();