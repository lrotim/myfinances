Date.prototype.copy = function() {
	var clone = new Date();
	clone.setMilliseconds(this.getMilliseconds());
	return clone;
}
if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    };
}