Date.prototype.copy = function() {
	var clone = new Date();
	clone.setMilliseconds(this.getMilliseconds());
	return clone;
}