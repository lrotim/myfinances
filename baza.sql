CREATE TABLE systemuser(
	username	char(30) PRIMARY KEY,
	password	char(40) NOT NULL
);

CREATE TABLE account(
	accnum SERIAL PRIMARY KEY
);

CREATE TABLE transaction(
	tranid 		SERIAL PRIMARY KEY,
	descr		char(30) NOT NULL,
	value		decimal(2) NOT NULL,
	note		char(255),
	username	char(30) NOT NULL,
	sumtototal	CHAR(1) NOT NULL,
	accnum		INT,
	FOREIGN KEY (username) references systemuser(username),
	FOREIGN KEY (accnum) references account(accnum)
);

CREATE TABLE income(
	tranid 		SERIAL PRIMARY KEY,
	reccurrence	char(10) NOT NULL,
	trandate	DATE NOT NULL,
	FOREIGN KEY (tranid) references transaction(tranid)
);

CREATE TABLE expense(
	tranid SERIAL PRIMARY KEY,
	FOREIGN KEY (tranid) references transaction(tranid)
);

CREATE TABLE category(
	catid	SERIAL UNIQUE,
	username	char(30),
	name	char(30) NOT NULL,
	PRIMARY KEY (username,name),
	FOREIGN KEY (username) references systemuser(username)
);

CREATE TABLE isInCategory(
	catidParent	SERIAL,
	catidChild	SERIAL,
	PRIMARY KEY (catidParent,catidChild),
	FOREIGN KEY (catidParent) references category(catid),
	FOREIGN KEY (catidChild) references category(catid)
);

CREATE TABLE transInCat (
	tranid SERIAL,
	catid SERIAL,
	PRIMARY KEY (tranid,catid),
	FOREIGN KEY (tranid) references transaction(tranid),
	FOREIGN KEY (catid) references category(catid)
);

CREATE TABLE rate(
	tranid 		SERIAL,
	trandate	DATE,
	paid 		char(1),
	PRIMARY KEY (tranid,trandate),
	FOREIGN KEY (tranid) references transaction(tranid)
);