if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    };
}
$('p.menuItem').hover(function() {
    /* Stuff to do when the mouse enters the element */
    this.style.color = 'gray';
    this.style.background = '#CCFF99';
}, function() {
    /* Stuff to do when the mouse leaves the element */
    this.style.color = '#C0C0C0';
    this.style.background = '#333';
});
$('p.menuItem').css('cursor', 'pointer');
$("#finances").click(function(event) {
    // var ele = document.getElementById('container');
    // if (!ele) {
    //     ele = document.createElement("DIV");
    //     ele.setAttribute("id", "container");
    //     document.getElementById('containerFrame').innerHTML = "";
    //     document.getElementById('containerFrame').appendChild(ele);
    // }
    // ele.innerHTML = "<div id=\"incomes\" class=\"financesPick\"><img class=\"financesPick\" src=\"imgs/moneyBag.jpg\" alt=\"Incomes image\" width=\"200\" height=\"200\" />Incomes</div> <div id=\"expenses\" class=\"financesPick\"><img class=\"financesPick\" src=\"imgs/burningmoney.gif\" alt=\"Expenses image\" width=\"200\" height=\"200\"  />Expenses</div>     <div id=\"total\" class=\"financesPick\"><img class=\"financesPick\" src=\"imgs/dollarSign.jpeg\" alt=\"Total image\" width=\"200\" height=\"200\" />Total</div>";
    // $('div.financesPick').hover(function() {
    //     this.style.background = '#868A08';
    // }, function() {
    //     this.style.background = null;
    // });
    document.getElementById('containerFrame').innerHTML = "<div id=\"tabs\"><div id=\"tabFrame\"> <div id=\"incomes\" class=\"tab\">Incomes</div> <div id=\"expenses\" class=\"tab\">Expenses</div> <div id=\"total\" class=\"tab\">Total</div></div> </div>";
    $('.tab').click(function(event) {
        $('.tab').css('border-bottom-color', 'black');
        this.style.borderBottomColor = '#339933';
    });
    $('#incomes').click(function(event) {
        if (INCOMESCONTAINER === null) {
            INCOMESCONTAINER = new IncomesContainer();
        }
        // document.getElementById('containerFrame').removeChild(document.getElementById('containerFrame').lastChild);
        document.getElementById('containerFrame').appendChild(INCOMESCONTAINER.getContainerBodyNode());
        // INCOMESCONTAINER.getElements('income');
        INCOMESCONTAINER.initNewContainer();
        $('#incomeAddButton').css('cursor', 'pointer');
        $('#incomeAddButton').click(function(event) {
            if (INCOMESCONTAINER.addIncomeButtonClicked) {
                return;
            } else {
                INCOMESCONTAINER.addIncomeButtonClicked = true;
            }
            var addIncomeWindow = document.createElement("DIV");
            addIncomeWindow.setAttribute('id', 'addIncomeWindow');
            addIncomeWindow.innerHTML = INCOMESCONTAINER.getAddIncomeCode();
            document.getElementById('overlayCenter').appendChild(addIncomeWindow);
            $('button#submintAddIncome').click(function(event) {
                var errorString = "";
                var newIncome = {
                    tranid: undefined;
                    desc: $('#descriptionIncome').val().trim(),
                    value: $('#valueIncome').val(),
                    note: $('#noteIncome').val().trim(),
                    recurr: undefined,
                    totalSum: document.getElementById('totalSumIncome').checked,
                    date: $('#calendarLabel').html()
                };
                var radios = document.getElementsByName('recIncome');
                for (var i = 0, length = radios.length; i < length; i++) {
                    if (radios[i].checked) {
                        newIncome.recurr = radios[i].value;
                        break;
                    }
                }
                if (!INCOMESCONTAINER.newIncomeParser(newIncome.desc, 0)) {
                    errorString = errorString.concat('Unallowed input in description.<br>');
                }
                if (!INCOMESCONTAINER.newIncomeParser(newIncome.value, 1)) {
                    errorString = errorString.concat('Value must be a number.<br>');
                }
                if (!INCOMESCONTAINER.newIncomeParser(newIncome.note, 2)) {
                    errorString = errorString.concat('Unallowed characters in note filed.<br>');
                }
                if (errorString !== "") {
                    var elem = document.getElementById('addIncomeInputError');
                    if (!elem) {
                        elem = document.createElement("DIV");
                        elem.setAttribute('id', 'addIncomeInputError');
                        document.getElementById('overlayCenter').appendChild(elem);
                    }
                    elem.innerHTML = errorString;
                    return;
                }
                newIncome.value = parseFloat(newIncome.value, 10);
                INCOMESCONTAINER.addNewElement(newIncome, true);
                $('#overlay').hide('slow', function() {
                    document.getElementById('overlayCenter').innerHTML = "";
                    INCOMESCONTAINER.addIncomeButtonClicked = false;
                });
            });
            $('#cancelAddIncome').click(function(event) {
                $('#overlay').hide('slow', function() {
                    document.getElementById('overlayCenter').innerHTML = "";
                    INCOMESCONTAINER.addIncomeButtonClicked = false;
                });
            });
            $('#overlay').show('fast', function() {
                new Calendar('calUniQue123413513', 'calendarLabel', 'overlay');
                document.getElementById("descriptionIncome").focus();
            });;
        });
    });
    $('#expenses').click(function(event) {
        
    });
});
var INCOMESCONTAINER = null;