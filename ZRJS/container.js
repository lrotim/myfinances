function Container() {
    var containerHTML = document.createElement('DIV');
    containerHTML.setAttribute('id', 'incomesContainer');
    var dates = {
        currDate: new Date(),
        displayDate: new Date()
    };
    var dataAmountStart = 0;
    var dataAmountEnd = 6;
    var cDownLeft = 6;
    var cDownRight = 6;
    var monthNames = {
        0: 'January',
        1: 'February',
        2: 'March',
        3: 'April',
        4: 'May',
        5: 'June',
        6: 'July',
        7: 'August',
        8: 'September',
        9: 'October',
        10: 'November',
        11: 'December'
    };
    var elemId = {
        transId: 0,
        getId: function() {
            var ret = this.transId;
            this.transId+=1;
            return ret;
        }
    }
    //  containerHTML.innerHTML = "<div id=\"showMonthAndYear\">" + "<button id=\"showMonthAndYearLeft\"><b>&#45;</b></button><label id=\"showMonthAndYearLabel\">" + monthNames[dates.displayDate.getMonth()] + "-" + dates.displayDate.getFullYear() + "</label><button id=\"showMonthAndYearRight\"><b>&#43;</b></button> <div id=\"cAndAChooserWraper\" > <div><button>Category</button> <button>Account</button></div><div id=\"selectorWrapper\"></div></div></div>";
    this.submitNewElements = function(type) {
        // var os = require('os'); 
        var data = type + "\n[",
            len = newElements[dates.displayDate.getMonth() + "-" + dates.displayDate.getFullYear()].length,
            i;
        for (i = 0; i < len; i += 1) {
            data = data.concat(JSON.stringify(newElements[dates.displayDate.getMonth() + "-" + dates.displayDate.getFullYear()][i]));
            if (i !== len - 1) data = data.concat(",");
        }
        data = data.concat("]");
        var conn = new XMLHttpRequest();
        //SET REAL USERNAME AND PASSWORD
        conn.withCredentials = true;
        conn.open('POST', 'ZRserver', true);
        conn.setRequestHeader("Content-type", "text-plain");
        conn.send(data);
        conn.onreadystatechange = function() {
            if (conn.readyState === 4 && conn.status === 200) {
                //TO DO
                var pattern = "/(username:)(.+)\s/";
                alert(pattern.exec(document.cookie));
                alert(document.cookie);
            }
        };
        addNewElementsToUserElements();
        newElements = [];
    };
    var addNewElementsToUserElements = function() {
        var key, len, i;
        for (key in newElements) {
            len = newElements[key].length;
            for (i = 0; i < len; i += 1) {
                if (!userElements[key]) userElements[key] = new Array();
                userElements[key].push(newElements[key][i]);
            }
        }
    };
    var newElements = [];
    var userElements = [];
    this.addNewElementToHTML = {};
    this.loadElementsInContainer = function() {
        this.emptyContainer();
        var key = dates.displayDate.getMonth() + "-" + dates.displayDate.getFullYear();
        if (userElements[key]) {
            var len = userElements[key].length;
            for (var i = 0; i < len; i += 1) {
                this.addNewElementToHTML(userElements[key][i]);
            }
        }
    };
    this.addNewElement = function(newElement) {
        newElement.tranid = elemId.getId();
        var date = newElement.date.split("\."); //test it
        if (!newElements[(date[1] - 1) + "-" + date[2]]) {
            newElements[(date[1] - 1) + "-" + date[2]] = new Array();
        }
        newElements[(date[1] - 1) + "-" + date[2]].push(newElement);
        if (!userElements[(date[1] - 1) + "-" + date[2]]) {
            userElements[(date[1] - 1) + "-" + date[2]] = new Array();
        }
        userElements[(date[1] - 1) + "-" + date[2]].push(newElement);
        userElements[(date[1] - 1) + "-" + date[2]].sort(function(a, b) {
            var dateA = a.date.split("\.");
            var dateB = b.date.split("\.");
            if (dateA[1] - dateB[1] !== 0) {
                return yearA - yearB;
            } else {
                if (dateA[0] - dateB[0] !== 0) return dateA[0] - dateB[0];
                else {
                    if (a.desc > b.desc) return 1;
                    if (a.desc < b.desc) return -1;
                    // a must be equal to b
                    return 0;
                }
            }
        });
        this.loadElementsInContainer();
    };
    
    this.getElements = function(type) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'ZRserver', true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send('type=' + type + '&start=' + dataAmountStart + '&end=' + dataAmountEnd);
        dataAmountStart = dataAmountEnd;
        dataAmountEnd *= 2;
        cDownRight = dataAmountEnd - dataAmountStart;
        cDownLeft = cDownRight;
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var data = JSON.parse(xhr.responseText);
                var len = data.length;
                for (var i = 0; i < len; i += 1) {
                    var key = getKeyFromDate(data[i].date);
                    if (!userElements[key]) userElements[key] = new Array();
                    userElements[key].push(data[i]);
                }
            }
        };

        function getKeyFromDate(dateString) {
            var res = dateString.split('\.');
            var month = parseInt(res[1], 10) - 1;
            return month + "-" + res[2];
        }
    };
    this.initNewContainer = function() {
        var that = this;
        // this.getElements();
        $('#showMonthAndYearLabel').html(monthNames[dates.displayDate.getMonth()] + '-' + dates.displayDate.getFullYear());
        $('#showMonthAndYearRight').click(function(event) {
            if ((dates.displayDate.getMonth() + 1) % 12 == 0) {
                dates.displayDate.setFullYear(dates.displayDate.getFullYear() + 1);
            }
            dates.displayDate.setMonth((dates.displayDate.getMonth() + 1) % 12);
            $('#showMonthAndYearLabel').html(monthNames[dates.displayDate.getMonth()] + '-' + dates.displayDate.getFullYear());
            cDownRight -= 1;
            cDownLeft += 1;
            if (cDownRight === 0) {
                this.getElements();
            }
            that.loadElementsInContainer();
        });
        $('#showMonthAndYearLeft').click(function(event) {
            if ((dates.displayDate.getMonth() - 1) == -1) {
                dates.displayDate.setMonth(11);
                dates.displayDate.setFullYear(dates.displayDate.getFullYear() - 1);
            } else {
                dates.displayDate.setMonth(dates.displayDate.getMonth() - 1);
            }
            $('#showMonthAndYearLabel').html(monthNames[dates.displayDate.getMonth()] + '-' + dates.displayDate.getFullYear());
            cDownRight += 1;
            cDownLeft -= 1;
            if (cDownLeft === 0) {
                this.getElements();
            }
            that.loadElementsInContainer();
        });
    };
    this.emptyContainer = function() {
        containerHTML.innerHTML = "";
    };
    this.getContainerBodyNode = function() {
        dates.displayDate = new Date();
        return containerHTML;
    };
    this.appendHTMLStringCode = function(HTMLString) {
        containerHTML.innerHTML = containerHTML.innerHTML.concat(HTMLString);
    };
    this.addChildNodeToHTML = function(newChild) {
        containerHTML.appendChild(newChild);
    };
}

Container.prototype.changeVisibility = function(elemenId){
        var vis = $('#'+elemenId).css('visibility');
        if(vis === 'visible'){
            $('#'+elemenId).css('visibility', 'hidden');
        }else{
            $('#'+elemenId).css('visibility', 'visible');
        }
}