var SELECT = {
    generirajSate: function() {
        var satiText = "";
        return function() {
            var i = 1;
            if (satiText === "") {
                for (; i <= 24; i += 1) {
                    satiText = satiText.concat('<option>' + i + '</option>');
                }
                document.getElementById('izborSati').innerHTML = satiText;
            }
        }
    }(),
    changeBC: function() {
        var curr = true;
        return function() {
            if (curr) {
                $('#calendarLabel').css('background-color', 'yellow');
            } else {
                $('#calendarLabel').css('background-color', 'green');
            }
            curr = !curr;
        }
    }()
};
$('#izborSati').click(function(event) {
    SELECT.generirajSate();
});
$('#calendarLabel').hover(function() {
    SELECT.changeBC();
}, function() {
    SELECT.changeBC();
});
var datum = new Date();
var CALENDAR = new Calendar('calendar','calendarLabel');
CALENDAR.changeColors('#AAEDE6', null, '#0D46A8');

var req = new XMLHttpRequest();