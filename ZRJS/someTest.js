function Container() {
    var addIncomeHTML = "<table><tbody><tr><td>Description:</td><td><input id=\"descriptionIncome\" type=\"text\" size=\"19\"></td></tr><tr><td>Recurrence:</td><td><input type=\"radio\" name=\"recIncome\" checked=\"checked\" value=\"None\"> None <input type=\"radio\" name=\"recIncome\" value=\"Monthly\">Monthly<input type=\"radio\" name=\"recIncome\" value=\"Yearly\">Yearly</td></tr><tr><td>Value: </td><td><input id=\"valueIncome\" type=\"text\" value=\"0.00\" size=\"5\" >&nbsp;&nbsp;$</td></tr><tr><td>Note:</td><td><textarea id=\"noteIncome\" maxlength=\"95\"></textarea></td></tr><tr><td colspan=\"2\"><label id=\"calendarLabel\">asd</label></td></tr><tr><td colspan=\"2\"><input id=\"totalSumIncome\" checked=\"checked\" type=\"checkbox\"> Sum to total </td></tr><tr><td colspan=\"2\" align=\"right\"><button id=\"submintAddIncome\">Add income</button><button id=\"cancelAddIncome\">Cancel</button></tr></tbody></table>";
    var incomesHTML = document.createElement('DIV');
    var currDate = new Date();
    var displayDate = currDate.copy();
    var some = "text";
    var monthNames = {
        0: 'January',
        1: 'February',
        2: 'March',
        3: 'April',
        4: 'May',
        5: 'June',
        6: 'July',
        7: 'August',
        8: 'September',
        9: 'October',
        10: 'November',
        11: 'December'
    };
    incomesHTML.setAttribute('id', 'incomesContainer');
    incomesHTML.innerHTML = "<div id=\"showMonthAndYear\">" + "<button id=\"showMonthAndYearLeft\"><b>&#45;</b></button><label id=\"showMonthAndYearLabel\">" + monthNames[displayDate.getMonth()] + "-" + displayDate.getFullYear() + "</label><button id=\"showMonthAndYearRight\"><b>&#43;</b></button></div><div id =\"incomeAddButton\">Click here in order to add a new income.</div>";
    $('#showMonthAndYearLeft').click(function(event) {
       //TO DO 
    });
    $('#showMonthAndYearRight').click(function(event) {
       //TO DO 
    });
    this.submintNewElements = function() {
        //TO DO
        var conn = new XMLHttpRequest();
        conn.open('GET', 'server', true, 'admin', 'somePass');    
    }

    this.getText = undefined;

    //REMOVE
    this.getAddIncomeCode = function() {
        return addIncomeHTML;
    }
    var newElements = [];
    var userElements = [];
    this.addNewElement = function(newElement) {
        var newChild = document.createElement("DIV");
        var tmp1, tmp2, lastChild;
        newElements.push(newElement);
        lastChild = incomesHTML.lastChild;
        incomesHTML.removeChild(lastChild);
        // if (incomesHTML.firstChild.nodeType === 3) {
        //     lastChild = incomesHTML.firstChild;
        //     incomesHTML.innerHTML = "";
        // } else {
        //     lastChild = incomesHTML.lastChild;
        //     incomesHTML.removeChild(lastChild);
        // }
        newChild.setAttribute('class', 'anIncome');
        tmp1 = document.createElement("DIV");
        tmp1.setAttribute('class', 'anIncomeLeft');
        tmp2 = document.createElement("p");
        tmp2.setAttribute("class", "anIncomeP anIncomeDescription");
        tmp2.innerHTML = "<b>Description:</b> " + newElement.desc;
        tmp1.appendChild(tmp2);
        tmp2 = document.createElement("p");
        tmp2.setAttribute("class", "anIncomeP");
        tmp2.innerHTML = "<b>Recurrence:</b> " + newElement.recurr;
        tmp1.appendChild(tmp2);
        tmp2 = document.createElement("p");
        tmp2.setAttribute("class", "anIncomeP");
        tmp2.innerHTML = "<b>Value:</b> " + newElement.value;
        tmp1.appendChild(tmp2);
        newChild.appendChild(tmp1);
        tmp1 = document.createElement("DIV");
        tmp1.setAttribute('class', 'anIncomeRight');
        tmp2 = document.createElement('p');
        tmp2.setAttribute("class", "anIncomeP anIncomeNote");
        tmp2.innerHTML = "<b>Note:</b><br>" + newElement.note;
        tmp1.appendChild(tmp2);
        tmp2 = document.createElement('p');
        tmp2.setAttribute("class", "anIncomeP");
        tmp2.innerHTML = "<b>Date:</b>" + newElement.date;
        tmp1.appendChild(tmp2);
        newChild.appendChild(tmp1);
        incomesHTML.appendChild(newChild);
        if (lastChild) {
            incomesHTML.appendChild(lastChild);
        }
    }
    this.getIncomesDIVNode = function() {
        return incomesHTML;
    }
    this.addIncomeButtonClicked = false;
}
Container.prototype.newIncomeParser = function() {
    var parseDescription = /^(?:[A-Za-z]+)(?: *\w)*$/;
    var parseVale = /^(\d+)(\.\d+)?$/;
    var parseNote = /^(?:[!-\/:-@\[-`{-~\-\s\w]*)$/;
    return function(input, inputType) {
        var result;
        switch (inputType) {
            case 0:
                result = parseDescription.exec(input) !== null;
                break;
            case 1:
                result = parseVale.exec(input) !== null;
                break;
            case 2:
                result = parseNote.exec(input) !== null;
                break;
        }
        return result;
    }
}();