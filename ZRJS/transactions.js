var FINANCESAPP = {};
FINANCESAPP.incomesContainer = undefined;
FINANCESAPP.expensesContainer = getUserCategories();
FINANCESAPP.userCateogires = getUserAccounts();
FINANCESAPP.userAccounts = undefined;
FINANCESAPP.elementSelected = undefined;

function getUserCategories() {}
function getUserAccounts() {}

$('#incomes').click(function(event) {
    if (FINANCESAPP.elementSelected === 0) {
        return;
    }
    var conn = new XMLHttpRequest();
conn.open('GET', 'ZRserver', true);
conn.send();
conn.onreadystatechange = function() {
    if (conn.readyState === 4 && conn.status === 200) {
        alert(document.cookie);
    }
};
    FINANCESAPP.elementSelected = 0;
    if (FINANCESAPP.incomesContainer === undefined) {
        FINANCESAPP.incomesContainer = new IncomesContainer();
        // FINANCESAPP.incomesContainer.getElements('income');
    }
    document.getElementById('containerFrame').appendChild(FINANCESAPP.incomesContainer.getContainerBodyNode());
    FINANCESAPP.incomesContainer.initNewContainer();
    $('#topMenu').css('visibility', 'visible');
    $('#selectCatButt').click(function(event) {
        FINANCESAPP.incomesContainer.changeVisibility('catSelectorWrapper');
    });
    $('#selectAccButt').click(function(event) {
        FINANCESAPP.incomesContainer.changeVisibility('accSelectorWrapper');
    });
    // $('#incomeAddButton').hover(function() {
    //     this.style.color = '#071907';
    //     this.style.backgroundColor = '#CCFF99'
    // }, function() {
    //     this.style.backgroundColor = '#071907';
    //     this.style.color = '#CCFF99';
    // });
    // $('#incomeAddButton').css('cursor', 'pointer');
    $('#incomeAddButton').click(function(event) {
        if (FINANCESAPP.incomesContainer.addIncomeButtonClicked) {
            return;
        } else {
            FINANCESAPP.incomesContainer.addIncomeButtonClicked = true;
        }
        var addIncomeWindow = document.createElement("DIV");
        addIncomeWindow.setAttribute('id', 'addIncomeWindow');
        addIncomeWindow.innerHTML = FINANCESAPP.incomesContainer.getAddIncomeCode();
        document.getElementById('overlayCenter').appendChild(addIncomeWindow);
        $('button#submintAddIncome').click(function(event) {
            var errorString = "";
            var newIncome = {
                desc: $('#descriptionIncome').val().trim(),
                value: $('#valueIncome').val(),
                note: $('#noteIncome').val().trim(),
                recurr: undefined,
                totalSum: document.getElementById('totalSumIncome').checked,
                date: $('#calendarLabel').html()
            };
            var radios = document.getElementsByName('recIncome');
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) {
                    // do whatever you want with the checked radio
                    newIncome.recurr = radios[i].value;
                    // only one radio can be logically checked, don't check the rest
                    break;
                }
            }
            if (!FINANCESAPP.incomesContainer.newIncomeParser(newIncome.desc, 0)) {
                errorString = errorString.concat('Unallowed input in description.<br>');
            }
            if (!FINANCESAPP.incomesContainer.newIncomeParser(newIncome.value, 1)) {
                errorString = errorString.concat('Value must be a number.<br>');
            }
            if (!FINANCESAPP.incomesContainer.newIncomeParser(newIncome.note, 2)) {
                errorString = errorString.concat('Unallowed characters in note filed.<br>');
            }
            if (errorString !== "") {
                var elem = document.getElementById('addIncomeInputError');
                if (!elem) {
                    elem = document.createElement("DIV");
                    elem.setAttribute('id', 'addIncomeInputError');
                    document.getElementById('overlayCenter').appendChild(elem);
                }
                elem.innerHTML = errorString;
                return;
            }
            newIncome.value = parseFloat(newIncome.value, 10);
            FINANCESAPP.incomesContainer.addNewElement(newIncome, true);
            $('#overlay').hide('slow', function() {
                document.getElementById('overlayCenter').innerHTML = "";
                FINANCESAPP.incomesContainer.addIncomeButtonClicked = false;
            });
        });
        $('#cancelAddIncome').click(function(event) {
            $('#overlay').hide('slow', function() {
                document.getElementById('overlayCenter').innerHTML = "";
                FINANCESAPP.incomesContainer.addIncomeButtonClicked = false;
            });
        });
        $('#overlay').show('fast', function() {
            var datum = $('#showMonthAndYearLabel').html();
            var date = new Date();
            var polje = datum.split("\-");
            date.setMonth(Calendar.prototype.getMonthNumber.apply(null, [polje[0]]));
            date.setFullYear(polje[1]);
            var elem = document.createElement("DIV");
            elem.setAttribute("id", "calUniQue123413513");
            document.getElementById("overlayCenter").appendChild(elem);
            new Calendar('calUniQue123413513', 'calendarLabel', date);
            document.getElementById("descriptionIncome").focus();
        });;
    });
});
$('.tab').click(function(event) {
    $('.tab').css('border-bottom-color', 'black');
    this.style.borderBottomColor = '#339933';
});
// var date = new Date();
// var mon = Calendar.prototype.getMonthName.apply(null, [date.getMonth()]);
// $('#showMonthAndYearLabel').html(mon + "-" + date.getFullYear());
// var CONTAINER = new Container();
// CONTAINER.initNewContainer();
//HIDE DATE AND CAT AND ACC
$('#expenses').click(function(event) {});