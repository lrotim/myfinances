var Calendar = function(calendarIdInHTML, calendarLabel) {
    var date = new Date();
    var displayDate = date.copy();
    selectedDate = date.getDate();
    selectedMonth = date.getMonth();
    selectedYear = date.getYear();
    var daysInMonth = setDaysInMonth(date.getMonth(), date.getFullYear());
    var currPickedDate = date.getDate();
    this.monthNames = {
r cellColors = ['white', 'red', 'yellow'];

    function setDaysInMonth(mm, yyyy) {
        if ((mm == 3) || (mm == 5) || (mm == 8) || (mm == 10)) {
            daysInMonth = 30;
        } else {
            daysInMonth = 31
            if (mm == 1) {
                if (yyyy / 4 - parseInt(yyyy / 4) !== 0) {
                    daysInMonth = 28;
                } else {
                    daysInMonth = 29;
                }
            }
        }
        return daysInMonth;
    }
    var generateCalendarHTML = function() {
        var htmlText = "",
            i = 0,
            j = 0,
            k = 0;
        htmlText = "<table border=\"2px\" ><tbody><tr><td align=\"center\" colspan=\"7\" id=\"xyz1245\" ></td></tr>";
        htmlText = htmlText.concat('<tr><td>M</td><td>T</td><td>W</td><td>T</td><td>F</td><td>S</td><td>S</td></tr>');
        for (i = 0; i < 6; i += 1) {
            htmlText = htmlText.concat('<tr>');
            for (j = 0; j < 7; j += 1) {
                htmlText = htmlText.concat('<td class=\"' + calendarIdInHTML + '\" style=\"cursor:pointer\" >&nbsp;</td>');
                k += 1;
            }
            htmlText = htmlText.concat('</tr>');
        }
        htmlText = htmlText.concat('</tbody></table>');
        return htmlText;
    };
    this.selectedDateString = function() {
        return selectedDate + '.' + (selectedMonth + 1) + '.' + selectedYear;
    }
    this.addOneMonth = function() {
        if ((displayDate.getMonth() + 1) % 12 == 0) {
            displayDate.setFullYear(displayDate.getFullYear() + 1);
        }
        displayDate.setMonth((displayDate.getMonth() + 1) % 12);
        setDaysInMonth(displayDate.getMonth(), displayDate.getFullYear());
    };
    this.subOneMonth = function() {
        if ((displayDate.getMonth() - 1) == -1) {
            displayDate.setMonth(11);
            displayDate.setFullYear(displayDate.getFullYear() - 1);
        } else {
            displayDate.setMonth(displayDate.getMonth() - 1);
        }
        setDaysInMonth(displayDate.getMonth(), displayDate.getFullYear());
    };
    this.refreshCal = function() {
        var that = this;
        var tableRow = document.getElementById(calendarIdInHTML).firstChild.firstChild.firstChild;
        tableRow.firstChild.innerHTML = '<button id=\"' + calendarIdInHTML + 'Left' + '\"><b>&#45;</b></button>&nbsp;' + this.monthNames[displayDate.getMonth()] + '-' + displayDate.getFullYear() + '&nbsp;<button id=\"' + calendarIdInHTML + 'Right' + '\"><b>&#43;</b></button>';
        $('#' + calendarIdInHTML + 'Left').click(function(event) {
            that.subOneMonth();
            that.refreshCal();
        });
        $('#' + calendarIdInHTML + 'Right').click(function(event) {
            that.addOneMonth();
            that.refreshCal();
        });
        selectedMonth = displayDate.getMonth();
        selectedYear = displayDate.getFullYear();
        var tableCell,
            start = false,
            i = 0,
            firstDayOfTheMonth, br = 1;
        displayDate.setDate(1);
        firstDayOfTheMonth = displayDate.getDay() - 1;
        if (firstDayOfTheMonth === -1) {
            firstDayOfTheMonth = 6;
        };
        tableRow = tableRow.nextSibling;
        while (tableRow = tableRow.nextSibling) {
            tableCell = tableRow.firstChild;
            do {
                if (i % 7 === (firstDayOfTheMonth) || start) {
                    start = true;
                    firstDayOfTheMonth = -1;
                    if (br === daysInMonth) {
                        start = false;
                    }
                    tableCell.innerHTML = br;
                    if (date.getDate() === br && displayDate.getMonth() === date.getMonth() && displayDate.getYear() === date.getYear()) {
                        tableCell.style.backgroundColor = cellColors[2];
                    } else {
                        tableCell.style.backgroundColor = cellColors[0];
                    }
                    tableCell.style.cursor = 'pointer';
                    tableCell.onmouseover = function() {
                        this.style.backgroundColor = cellColors[1];
                    };
                    tableCell.onmouseout = function() {
                        if (date.getDate() === parseInt(this.innerHTML, 10) && displayDate.getMonth() === date.getMonth() && displayDate.getYear() === date.getYear()) {
                            this.style.backgroundColor = cellColors[2];
                        } else this.style.backgroundColor = cellColors[0];
                    }
                    tableCell.onclick = function() {
                        var date = this.innerHTML;
                        selectedDate = date;
                        $('#' + calendarLabel).html(that.selectedDateString());
                        that.changeVis();
                    }
                    br += 1;
                } else {
                    tableCell.innerHTML = '&nbsp;'
                    tableCell.style.backgroundColor = 'gray';
                    tableCell.style.cursor = 'default';
                    tableCell.onmouseover = null;
                    tableCell.onmouseout = null;
                    tableCell.onclick = null;
                }
                i += 1;
            } while (tableCell = tableCell.nextSibling);
        }
    };
    this.createCalendarHTML = function(called) {
        return function() {
            var that = this;
            if (called) {
                return false;
            }
            called = !called;
            var pos = $('#' + calendarLabel).position();
            var elem = document.createElement("DIV");
            elem.setAttribute("id", calendarIdInHTML);
            elem.innerHTML = generateCalendarHTML();
            document.body.appendChild(elem);
            $(elem).hide('fast');
            elem.style.visibility = 'hidden';
            $('#xyz1245').css('border', '0');
            $('#' + calendarIdInHTML).css({
                'top': (pos.top + 30) + 'px',
                'left': (pos.left) + 'px'
            });
            this.refreshCal();
            return true;
        }
    }(false);
    this.changeVis = function() {
        var vis = $('#' + calendarIdInHTML).css('visibility');
        vis === 'hidden' ? $('#' + calendarIdInHTML).show('slow') : $('#' + calendarIdInHTML).hide('fast');
        vis === 'hidden' ? $('#' + calendarIdInHTML).css('visibility', 'visible') : $('#' + calendarIdInHTML).css('visibility', 'hidden');
        vis = $('#' + calendarIdInHTML).css('visibility');
        if (vis === 'hidden') {
            displayDate = date.copy();
            this.refreshCal();
        }
    }
    this.changeColors = function(cellColor, cellColorOnMouseOver, currentDateColor) {
        var i = 0;
        for (; i < 3; i += 1) {
            if (arguments[i]) cellColors[i] = arguments[i];
        }
        this.refreshCal();
    };
    this.createCalendarHTML(calendarLabel);
};
Calendar.prototype.monthNames = {
    0: 'January',
    1: 'February',
    2: 'March',
    3: 'April',
    4: 'May',
    5: 'June',
    6: 'July',
    7: 'August',
    8: 'September',
    9: 'October',
    10: 'November',
    11: 'December'
};